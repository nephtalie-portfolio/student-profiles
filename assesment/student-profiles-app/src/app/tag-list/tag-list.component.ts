import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { isNil } from 'lodash';
import { SearchService } from '../services/search.service';
import { StudentService } from '../services/student.service';
import { Tag } from '../models/tag';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit {

  public tagInput = new FormControl('');
  @Input() public studentId! : string;
  @Input() public tags : Tag[] = [];

  constructor(public searchService: SearchService, private studentService: StudentService) { 
    this.tags = this.studentService.getStudentTags(this.studentId);
  }

  ngOnInit(): void {
  }

  
  public addTag(event:any){
    if(event.keyCode == 13 ){ // 13 is the keycode for enter
      if(this.tagInput.value != "" && !isNil(this.tagInput.value)){
        this.studentService.addStudentTag({studentId:this.studentId,tag:this.tagInput.value})
        this.tags = this.studentService.getStudentTags(this.studentId);
        this.tagInput.reset();
    }
    }
    
  }

}
