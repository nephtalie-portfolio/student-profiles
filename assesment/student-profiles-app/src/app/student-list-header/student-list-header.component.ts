import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'student-list-header',
  templateUrl: './student-list-header.component.html',
  styleUrls: ['./student-list-header.component.scss']
})
export class StudentListHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
