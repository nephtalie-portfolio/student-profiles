import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-collapsible',
  templateUrl: './collapsible.component.html',
  styleUrls: ['./collapsible.component.scss']
})
export class CollapsibleComponent implements OnInit {
  @Input() public openByDefault: boolean = false;
  @Input() public canExpand: boolean = true;
  @Output() public collapsibleToggled: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() public badgeStyle?: string = '';
  public open: boolean = false;

  public onToggle(): void {
    if (this.canExpand) {
      this.open = !this.open;
      this.collapsibleToggled.emit(this.open);
    }
  }
  public ngOnInit(): void {
    this.open = this.openByDefault;
  }
}
