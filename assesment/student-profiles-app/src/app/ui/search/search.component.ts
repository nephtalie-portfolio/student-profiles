import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
@Output() public searched = new EventEmitter();
@Input() public placeholder? :string;

  public searchBar = new FormControl('');
  constructor() { }

  search(){
    this.searched.emit(this.searchBar.value);
  }

  ngOnInit(): void {
  }

}
