import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchService } from '../services/search.service';
import { Student } from '../models/student';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {

  public students: Observable<Student[]>;

  constructor(private studentService : StudentService, public searchService: SearchService) {
     this.students = this.studentService.getStudentList();
   }
  

  public ngOnInit(): void {
  }

  public getStudentTags(student:string){
    return this.studentService.getStudentTags(student);
  }
  
  

}
