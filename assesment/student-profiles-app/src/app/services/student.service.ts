import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Student } from '../models/student';
import { Observable, Subject } from 'rxjs';
import {  map } from 'rxjs/operators';
import { Tag } from '../models/tag';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private studentUrl = 'https://api.hatchways.io/assessment/students';
  
  studentList$ : Observable<Student[]>;
  studentTags : Tag[]=[];

  constructor(private http: HttpClient) { 
    this.studentList$ = this.http.get<Observable<Student[]>>(this.studentUrl).pipe(map((val:any)=>val.students));
  }

   getStudentList() :Observable<Student[]>{
    return this.studentList$;
   }

   addStudentTag(tag:Tag){
    this.studentTags.push(tag);
   }

   getStudentTags(studentId:string):Tag[]{
    return this.studentTags.filter(tag=>tag.studentId ==studentId);
   }



   

}
