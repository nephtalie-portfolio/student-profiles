import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  public searchedStudent = ""; 
  public searchedTag ="";

  constructor() { }


  getSearchedStudent(){
    return this.searchedStudent;
  }
  setSearchedStudent(student:string){
    this.searchedStudent = student;
  }
  
  getSearchedTag(){
    return this.searchedTag;
  }
  setSearchedTag(searchedTag:string){
    this.searchedTag = searchedTag;
  }
}
