import { Pipe, PipeTransform } from '@angular/core';
import { Student } from '../models/student';
import { StudentService } from '../services/student.service';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  constructor(){}
  transform(value:any, search:string): any {
    
    if(!search){return value;}
    
    return this.getStudentList(value,search);

  }
 
  // Gets the list of students that match search :

  getStudentList(students:Student[]|null,search:string):Student[]{
  
    return students!.filter((student:Student)=>{
     
      if(!student){return}
      let searchLowerCase = search.toLowerCase();
       if(student.firstName.toLowerCase().indexOf(searchLowerCase)!== -1
       || student.lastName.toLowerCase().indexOf(searchLowerCase) !== -1){
          return true;
       }else{
         return false;
       }

    });
  }
}
