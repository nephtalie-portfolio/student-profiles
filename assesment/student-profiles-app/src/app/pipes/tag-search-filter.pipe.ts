import { Pipe, PipeTransform } from '@angular/core';
import { Student } from '../models/student';
import { StudentService } from '../services/student.service';
import { Tag } from '../models/tag';

@Pipe({
  name: 'tagSearchFilter'
})
export class TagSearchFilterPipe implements PipeTransform {

  constructor(private studentService:StudentService){}
  transform(students: Student[],tag:string):Student[]{
    if(tag == ""){return students;}

    return this.getStudentsWithTag(students,tag);
    
  }

  // Gets the list of student that have a tag that matches the search

  getStudentsWithTag(students:Student[],searchedTag:string){

    return students.filter((student:Student)=>{
      let tags = this.studentService.getStudentTags(student.id);

      if(!tags){return}
      //Returns the array of tags that matched the tag search. 
      let resultTags = tags.filter(((tag:Tag)=>tag.tag.indexOf(searchedTag) !== -1));

      return resultTags.length !=0;
    });


  }

 

}
