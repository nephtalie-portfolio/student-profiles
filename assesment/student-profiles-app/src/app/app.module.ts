import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentComponent } from './student/student.component';
import { SearchFilterPipe } from './pipes/search-filter.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { CollapsibleComponent } from './ui/collapsible/collapsible.component';
import { SearchComponent } from './ui/search/search.component';
import { TagListComponent } from './tag-list/tag-list.component';
import { TagComponent } from './tag/tag.component';
import { StudentListHeaderComponent } from './student-list-header/student-list-header.component';
import { TagSearchFilterPipe } from './pipes/tag-search-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    StudentListComponent,
    StudentComponent,
    SearchFilterPipe,
    CollapsibleComponent,
    SearchComponent,
    TagListComponent,
    TagComponent,
    StudentListHeaderComponent,
    TagSearchFilterPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
