import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Student } from '../models/student';
import { Tag } from '../models/tag';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit, OnChanges {
 @Input() public student! : Student;
 @Input() public tags?: Tag[];
 public average : number = 0;

  constructor() { }

  public ngOnInit(): void {
  }

  public ngOnChanges(changes: SimpleChanges):void{
    this.average = this.getAverage();
  }

  public getAverage() : number{
    let sumScore=0;

    this.student?.grades.forEach(score =>{
      sumScore += parseInt(score);
    });

    return sumScore / this.student!.grades.length;
  }


}
